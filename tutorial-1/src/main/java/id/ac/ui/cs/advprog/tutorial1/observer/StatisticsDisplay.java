package id.ac.ui.cs.advprog.tutorial1.observer;

import java.util.Observable;
import java.util.Observer;

public class StatisticsDisplay implements Observer, DisplayElement {

    private float maxTemp = 0.0f;
    private float minTemp = 200;
    private float tempSum = 0.0f;
    private int numReadings;

    public StatisticsDisplay(Observable observable) {
        observable.addObserver(this);
    }

    @Override
    public void display() {
        System.out.println("Avg/Max/Min temperature = " + (tempSum / numReadings)
                + "/" + maxTemp + "/" + minTemp);
    }

    public float getMaxTemp() {
        return maxTemp;
    }

    public void setMaxTemp(float maxTemp) {
        if (maxTemp > this.getMaxTemp()) {
            this.maxTemp = maxTemp;
        }
    }

    public float getMinTemp() {
        return minTemp;
    }

    public void setMinTemp(float minTemp) {
        if (minTemp < this.getMinTemp()) {
            this.minTemp = minTemp;
        }
    }

    public float getTempSum() {
        return tempSum;
    }

    public void setTempSum(float tempSum) {
        this.tempSum = tempSum;
    }

    public int getNumReadings() {
        return numReadings;
    }

    public void setNumReadings(int numReadings) {
        this.numReadings = numReadings;
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof WeatherData) {
            WeatherData data = (WeatherData) o;
            float curTemp = data.getTemperature();
            this.setTempSum(this.getTempSum() + curTemp);
            this.setNumReadings(this.getNumReadings() + 1);
            this.setMaxTemp(curTemp);
            this.setMinTemp(curTemp);
            this.display();
        }
    }
}
